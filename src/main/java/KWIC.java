import java.util.*;

public class KWIC {
    public static ArrayList<String> Ignored(String input) {
        ArrayList<String> iws = new ArrayList<String>(); //iws = ignored words
        Scanner s = new Scanner(input);
        while (s.hasNextLine()) {
            String string = s.nextLine();
            if (string.equals("::")) {
                break;
            }
            else
                iws.add(string);
        }
        return iws;//get the ignored words
    }

    public static ArrayList<String> Title(String input) {
        ArrayList<String> ts = new ArrayList<String>(); //ts = titles
        Scanner s = new Scanner(input);
        while (s.hasNextLine()) {
            String string = s.nextLine();
            if (string.equals("::")) {
                break;
            }
        }
        while (s.hasNextLine()) {
            String string_2 = s.nextLine();
            ts.add(string_2);
        }
        return ts; //get the titles
    }
    public static Map<String, ArrayList<String>> Keywords = new HashMap<String, ArrayList<String>>();

    public static Map<String, ArrayList<String>> findKey(List<String> ignored, List<String> title) {
        for (String s : title) {
            s = s.toLowerCase();
            ArrayList<String> wd = new ArrayList<>();
            wd.addAll(Arrays.asList(s.split("\\s+")));
            int index = 0;
            for (String w : wd) {
                ArrayList<String> Neword = new ArrayList<>(wd);
                Neword.set(index, w.toUpperCase());
                String new_s = String.join(" ", Neword);
                if (!ignored.contains(w)) {
                    if (Keywords.containsKey(w))
                        Keywords.get(w).add(new_s);
                    else Keywords.put(w, new ArrayList<String>(Arrays.asList(new_s)));
                }
                index++;
            }
        }

        return Keywords;
    }

    public static Map<String, ArrayList<String>> sortKey(Map<String, ArrayList<String>> map) {
        return new TreeMap<String, ArrayList<String>>(map);
    }

    public static String Result(Map<String, ArrayList<String>> map) {
        String string = "";
        for (String key : map.keySet()) {
            for (String title : map.get(key)) {
                string = string + title + "\n";
            }
        }
        return string;
    }
}

