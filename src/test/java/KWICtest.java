import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Disabled;
import static org.junit.jupiter.api.Assertions.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

class KWICTest {
    @Test

    void Test_Ignored() {
        String input = "is\n" +
                "the\n" +
                "::\n" +
                "Descent of Man\n" +
                "The Ascent of Man\n" +
                "The Old Man and The Sea\n" +
                "A Portrait of The Artist As a Young Man\n" +
                "A Man is a Man but Bubblesort IS A DOG\n";
        ArrayList<String> expected = new ArrayList<>();
        expected.add("is");
        expected.add("the");
        assertEquals(new KWIC().Ignored(input), expected);
    }

    @Test

    void Test_Titles() {
        String input = "is\n" +
                "the\n" +
                "::\n" +
                "Descent of Man\n" +
                "The Ascent of Man\n";
        ArrayList<String> expected = new ArrayList<>();
        expected.add("Descent of Man");
        expected.add("The Ascent of Man");
        assertEquals(new KWIC().Title(input), expected);
    }

    ArrayList<String> ignores;
    ArrayList<String> titles;
    public static Map<String, ArrayList<String>> Keywords = new HashMap<String, ArrayList<String>>();
    @Test

    void Test_Keywords() {
        String input = "is\n" +
                "the\n" +
                "::\n" +
                "Descent of Man\n" +
                "The Ascent of Man\n";
        ignores = KWIC.Ignored(input);
        titles = KWIC.Title(input);
        Keywords = KWIC.findKey(ignores, titles);
        Keywords = KWIC.sortKey(Keywords);
        String expected = ("the ASCENT of man\n" + "DESCENT of man\n" + "descent of MAN\n" + "the ascent of MAN\n" +
                "descent OF man\n" + "the ascent OF man\n");
        assertEquals(expected, new KWIC().Result(Keywords));
    }

    @Test

    void Test_Whole_Keywords() {
        String input = "is\n" +
                "the\n" +
                "of\n" +
                "and\n" +
                "as\n" +
                "a\n" +
                "but\n" +
                "::\n" +
                "Descent of Man\n" +
                "The Ascent of Man\n" +
                "The Old Man and The Sea\n" +
                "A Portrait of The Artist As a Young Man\n" +
                "A Man is a Man but Bubblesort IS A DOG\n";
        ignores = KWIC.Ignored(input);
        titles = KWIC.Title(input);
        Keywords = KWIC.findKey(ignores, titles);
        Keywords = KWIC.sortKey(Keywords);
        String expected = "a portrait of the ARTIST as a young man\n" +
                "the ASCENT of man\n" +
                "a man is a man but BUBBLESORT is a dog\n" +
                "DESCENT of man\n" +
                "a man is a man but bubblesort is a DOG\n" +
                "descent of MAN\n" +
                "the ascent of MAN\n" +
                "the old MAN and the sea\n" +
                "a portrait of the artist as a young MAN\n" +
                "a MAN is a man but bubblesort is a dog\n" +
                "a man is a MAN but bubblesort is a dog\n" +
                "the OLD man and the sea\n" +
                "a PORTRAIT of the artist as a young man\n" +
                "the old man and the SEA\n" +
                "a portrait of the artist as a YOUNG man\n";
        assertEquals(expected, new KWIC().Result(Keywords));
    }
}




